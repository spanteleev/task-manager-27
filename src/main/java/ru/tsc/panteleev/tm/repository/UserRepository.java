package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.IUserRepository;
import ru.tsc.panteleev.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return models.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return models.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst().orElse(null);
    }

}
